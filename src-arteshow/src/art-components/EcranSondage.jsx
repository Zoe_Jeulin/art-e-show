import '../art-css/sondage.css';
import logo from '../logo_arteshow_blanc.png';

import tmi from "tmi.js";
import { useParams } from "react-router-dom";
import React, { useEffect, useState } from 'react';

const EcranSondage = () => {
    const [list, setList] = useState([]);
    const [totalVotes, setTotalVotes] = useState(0);
    const [nbVote1, setNbVote1] = useState(0);
    const [nbVote2, setNbVote2] = useState(0);
    // const [pourcentage1, setPourcentage1] = useState(0);
    // const [pourcentage2, setPourcentage2] = useState(0);

    useEffect(() => {
        const client = new tmi.Client({
            channels: [ 'jln_zoe' ]
        });
        client.connect();
        client.on('message', (channel, tags, message, self) => {
            console.log(`${tags['display-name']}: ${message}`);
            const chatMsg = {
                'time': Date.now(),
                'user': tags['display-name'],
                'txt': message
            }
            setList(list => [...list,chatMsg]);
            if(chatMsg.txt === "1"){
                setTotalVotes(totalVotes => totalVotes+1);
                setNbVote1(nbVote1 => nbVote1+1);
                // setPourcentage1(pourcentage1 => nbVote1/totalVotes*100);
            }
            if(chatMsg.txt === "2"){
                setTotalVotes(totalVotes => totalVotes+1);
                setNbVote2(nbVote2 => nbVote2+1);
                // setPourcentage2(pourcentage2 => nbVote2/totalVotes*100);
            }
        });
    }, []);
    
    const params = useParams();
    
    return(
        <div id="div_background" style={{backgroundImage: `url("/${params.art}/layouts/layout_${params.art}_2.png")`}}>
            <div id="div_header_sondage">
                 <img src={logo} alt="Logo" class="logo"></img>
                <h1 id="h1_sondage">La semaine prochaine...</h1>
            </div>            
            <div id="div_contenu">
                <div id="div_sondage">
                    <div id="div_num_choix">
                        <h2>1</h2>
                        <h2>2</h2>
                    </div>
                    <div id="div_noms_choix">
                        <h3>{params.choix1}</h3>
                        <h3>{params.choix2}</h3>
                    </div>
                    <div id="div_jauges">
                        <div id="zone_jauge1">
                            <div id="jauge1" style={{height: totalVotes === 0 ? 0 : (nbVote1/totalVotes*100)*5}}></div>
                        </div>
                        <div id="zone_jauge2">
                            <div id="jauge2" style={{height: totalVotes === 0 ? 0 : (nbVote2/totalVotes*100)*5}}></div>
                        </div>
                    </div>
                    <div id="div_pourcentages">
                        <p>{totalVotes === 0 ? 0 : (nbVote1/totalVotes*100).toString().substring(0,3)} %</p>
                        <p>{totalVotes === 0 ? 0 : (nbVote2/totalVotes*100).toString().substring(0,3)} %</p>
                    </div>
                </div>            

                <div id="div_partie_droite">
                    <div id="div_webcam">
                    </div>
                    <div id="div_chat">
                        <ul id="ul_chat">
                            {list.slice(Math.max(list.length - 12, 0)).map((msg) => <li key={msg.time+msg.user+msg.txt} className="li_sondage">{msg.user} : {msg.txt}</li>)}
                        </ul>
                        {/* <iframe src="https://www.twitch.tv/embed/jln_zoe/chat?parent=localhost" width="500" height="300" title="Chat"></iframe> */}
                    </div>
                </div>
                
            </div>            
        </div>        
    );
}

export default EcranSondage;

/*
VERSION AVEC FETCH FAKE JSON

const handleMsg = () => {
    if(listeMsg[idMsg] !== undefined){
        if((listeMsg[idMsg]).message == "1"){
            setTotalVotes(totalVotes+1);
            setNbVote1(nbVote1+1);
            setPourcentage1(nbVote1/totalVotes*100);
        }
        if((listeMsg[idMsg]).message == "2"){
            setTotalVotes(totalVotes+1);
            setNbVote2(nbVote2+1);
            setPourcentage2(nbVote2/totalVotes*100);
        }
    }
}

const handleTxt = (message) => {
    const newTxt = message;
    setText(newTxt);
    handleList();
}

const handleList = (message) => {
    const newList = list.concat(text)
    setList(newList);
}

useEffect(() => {
    fetch('http://www.json-generator.com/api/json/get/ceqgsFvuKW?indent=2')
        .then(response => response.json())
        .then((msg) => setListeMsg( msg ));
}, []);

useEffect(() => {
    const interval = setInterval(() => {
        handleMsg();
        setIdMsg(idMsg+1);
    }, 250);
    return () => clearInterval(interval);
});
*/