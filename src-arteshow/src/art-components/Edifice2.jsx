import '../art-css/scene2_edifice.css';
// import edifices from '../art-json/edifices.json';
import React, { useEffect, useState } from 'react';

const Edifice2 = (props) => {
    const [edifices, setEdifices] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceCfyHMAya?indent=2')
            .then((res) => res.json())
            .then((data) => setEdifices(data));
    }, []);
    
    const edifice = edifices[props.id];

    if(edifice !== undefined){
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/architecture/layouts/layout_architecture_2.png\")"}}>
                <h1>Lundi archi'</h1>
                <div class="div_contenu_map">
                    <div class="div_map">
                        <iframe className="map" src={edifice.url_map} title="Carte"></iframe>
                    </div>
                    <div class="div_nom_lieu">
                        <p className="nom_lieu">Situé à {edifice.lieu}</p>
                    </div>
                </div>                                              
            </div>
        );
    }else{
        return null;
    }
}

export default Edifice2;