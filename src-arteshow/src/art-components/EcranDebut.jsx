import '../art-css/ecran_debut_fin.css';
import logo from '../logo_arteshow_blanc.png';
import { useParams } from "react-router-dom";
import Countdown from 'react-countdown';

const getJour = (art) => {
    switch(art){
        case "architecture":
            return <p>Lundi archi'</p>;
        case "cinema":
            return <p>Mardi ciné</p>;
        case "litterature":
            return <p>Mercredi lecture</p>;
        case "musique":
            return <p>Jeudi musique</p>;
        case "peinture":
            return <p>Vendredi peinture</p>;
        case "theatre":
            return <p>Samedi théâtre</p>;
        case "sculpture":
            return <p>Dimanche sculpture</p>;
        default:
            return <p></p>;       
    }                   
}

const EcranDebut = () => {
    const params = useParams();

    var currentDate = new Date();

    var yearCountdown = currentDate.getFullYear();
    var monthCountdown = ("0"+(currentDate.getMonth()+1)).slice(-2);
    var dayCountdown = ("0"+currentDate.getDate()).slice(-2);
    var hourCountdown = ("0"+params.heures).slice(-2);
    var minuteCountdown = ("0"+params.minutes).slice(-2);

    var dateCountdown = `${yearCountdown}-${monthCountdown}-${dayCountdown}T${hourCountdown}:${minuteCountdown}:00`;

    console.log(dateCountdown);

    return(
        <div id="div_background">
            <span id="content">
                <h2 class="scenese_h2 down">Le live ART'e-show commencera dans :</h2>
                <p class="timer"><Countdown date={dateCountdown}>
                <p class="timer">Le live va bientôt commencer !</p>
                </Countdown></p>
                <p class="day">{getJour(params.art)}</p>
                <img src={logo} alt="Logo" class="day scenese_img"></img>
            </span>
        </div>
    );
}

export default EcranDebut;
