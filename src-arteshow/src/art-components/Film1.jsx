import '../art-css/scene1.css';
import logo from '../logo_arteshow_blanc.png';
// import films from '../art-json/films.json';
import React, { useEffect, useState } from 'react';

const Film1 = (props) => {
    const [films, setFilms] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceAcVUUGmq?indent=2')
            .then((res) => res.json())
            .then((data) => setFilms(data));
    }, []);

    const film = films[props.id];

    if(film !== undefined){
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/cinema/layouts/layout_cinema_1.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Mardi ciné</h1>
                <h2 className="scene1_h2">{film.titre}</h2>
                <img id="scene1_img" src={`/cinema/images/${film.url_photo}`} alt={film.titre}></img>
                <div id="real">
                    <p>Réalisé par <span>{film.realisateur.nom}</span></p>
                    <img src={`/cinema/images/${film.realisateur.url_photo}`} alt={film.realisateur.nom}></img>
                </div>
                <div id="desc">
                    <p>Film sorti en {film.date}</p>
                    <ul>
                        {film.genres.length > 1 ? <p id="genre">Genres :</p> : <p id="genre">Genre :</p>}                        
                        {film.genres.map( (genre) => (
                            <li key={genre}>
                                <p>{genre}</p>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }else{
        return null;
    }
}
  
export default Film1;