import '../art-css/scene1.css';
import logo from '../logo_arteshow_blanc.png';
// import pieces from '../art-json/pieces.json';
import React, { useEffect, useState } from 'react';

const Piece1 = (props) => {
    const [pieces, setPieces] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/cflbFlEzqW?indent=2')
            .then((res) => res.json())
            .then((data) => setPieces(data));
    }, []);

    const piece = pieces[props.id];

    if(piece !== undefined){ 
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/theatre/layouts/layout_theatre_1.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Samedi théâtre</h1>
                <h2 class="scene1_h2">{piece.titre}</h2>
                <img src={`/theatre/images/${piece.url_photo}`} alt={piece.titre} id="scene1_img" ></img>
                <div id="real">
                    <h3>Une pièce de <span>{piece.auteur.nom}</span></h3>
                    <img src={`/theatre/images/${piece.auteur.url_photo}`} alt={piece.auteur.nom} />
                </div>
                {/*
                <div id="comediens">
                    <ul>
                        <h4>Les comédiens</h4>
                        {piece.comediens.map( (comedien) => comedien.url_photo!=null ?
                            (
                                <li key={comedien.nom}>
                                    <p>{comedien.nom}</p>
                                    <img src={`/theatre/images/comediens/${comedien.url_photo}`} alt={comedien.nom} />
                                </li>
                            ):(
                                <li key={comedien.nom}>
                                    <p>{comedien.nom}</p>
                                </li>
                            )
                        )}
                    </ul>
                </div>
                */}
                <div id="desc">
                    <p>Pièce sortie en  :<span>{piece.date}</span></p>
                    <ul>
                        {piece.styles.length > 1 ? <p id="genre">Genres :</p> : <p id="genre">Genre :</p>}                        
                        {piece.styles.map( (style) => (
                            <li key={style}>
                                <p>{style}</p>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }else{
        return null;
    }
}

export default Piece1;