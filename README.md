# ART'e-show

## Le projet

Un projet React réalisé par Aloïs AUBERT, Loona GAILLARD, Esther GARTNER, Clothilde HUVIER et Zoé JEULIN, dans le cadre du cours de web de 2ème année d'IMAC.

ART'e-show est une émission fictive quotidienne en streaming sur Twitch, présentant chaque jour une oeuvre d'un domaine différent des 7 arts (cinéma, musique, littérature, sculpture, architecture, théâtre, peinture).

Le projet était de réaliser dynamiquement les différents composants et scène de notre stream, en utilisant React.

## Installation et lancement

Après avoir cloné ou téléchargé le projet, se placer dans le dossier `/src-arteshow` et installer les dépendances du projet avec **`npm install`**.
Lancer le projet avec **`npm run start`** : une fenêtre de navigateur à l'adresse `localhost:3000` devrait s'ouvrir.

## URLs pour afficher les différentes scènes 

- Scène de compte à rebours avant le début du live : `localhost:3000/[art]/start/[heures]/[minutes]`
    - `[art]` : *architecture*, *cinema*, *litterature*, *musique*, *peinture*, *sculpture*, *theatre*
    - `[heures]` : nombre de 1 (ou 01) à 23
    - `[minutes]` : nombre de 0 (ou 00) à 59

        *exemple : `localhost:3000/cinema/start/20/30` pour un compte à rebours qui se termine à 20h30*

- 1ère scène de présentation d'une oeuvre : `localhost:3000/[art]/1`
    - `[art]` : *architecture*, *cinema*, *litterature*, *musique*, *peinture*, *sculpture*, *theatre*

        *exemple : `localhost:3000/cinema/1`*

- 2nde scène de présentation d'une oeuvre : `localhost:3000/[art]/2`
    - `[art]` : *architecture*, *cinema*, *litterature*, *musique*, *peinture*, *sculpture*, *theatre*

        *exemple : `localhost:3000/cinema/2`*

- Scène de sondage, pour permettre aux spectateurs de voter entre 2 oeuvres celle qui serait présentée lors de l'émission suivante : `localhost:3000/[art]/sondage/[choix1]/[choix2]`
    - `[art]` : *architecture*, *cinema*, *litterature*, *musique*, *peinture*, *sculpture*, *theatre*
    - `[choix1]`, `[choix2]` : titre des oeuvres pour lesquelles le vote sera possible

        *exemple : `localhost:3000/cinema/sondage/Astérix Mission Cléopâtre/La cité de la peur`*

    **Remarque** : les spectateurs votent en envoyant 1 ou 2 sur le tchat de la chaîne Twitch https://www.twitch.tv/jln_zoe

- Scène de fin de stream : `localhost:3000/end`
