import '../art-css/scene1.css';
import logo from '../logo_arteshow_blanc.png';
// import musiques from '../art-json/musiques.json';
import React, { useEffect, useState } from 'react';

const Musique1 = (props) => {
    const [musiques, setMusiques] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/cjkBUReLfS?indent=2')
            .then((res) => res.json())
            .then((data) => setMusiques(data));
    }, []);

    const musique = musiques[props.id];

    if(musique !== undefined){ 
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/musique/layouts/layout_musique_1.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Jeudi musique</h1>
                <h2 class="scene1_h2">{musique.titre}</h2>
                <div>
                    <figure>
                        <img id="scene1_img" src={`/musique/images/${musique.url_photo}`} alt={musique.album}></img>
                        <figcaption>Album <span>{musique.album}</span></figcaption>
                    </figure>
                </div>
                <div id="real">
                    <p>Réalisé par <span>{musique.artiste.nom}</span></p>
                    <img  src={`/musique/images/${musique.artiste.url_photo}`} alt={musique.artiste}></img>
                </div>
               
                <div id="desc">
                    <p>Album sorti en : <span>{musique.date}</span></p>
                    <ul>
                        {musique.styles.length > 1 ? <p id="genre">Genres :</p> : <p id="genre">Genre :</p>}                        
                        {musique.styles.map( (style) => (
                            <li key={style}>
                                <p>{style}</p>
                            </li>
                        ))}
                    </ul>
                </div>                
            </div>
        );
    }else{
        return null;
    }
}

export default Musique1;