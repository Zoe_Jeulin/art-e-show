import '../art-css/scene1.css';
import logo from '../logo_arteshow_blanc.png';
// import peintures from '../art-json/peintures.json';
import React, { useEffect, useState } from 'react';

const Peinture1 = (props) => {
    const [peintures, setPeintures] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceOGzVdNgy?indent=2')
            .then((res) => res.json())
            .then((data) => setPeintures(data));
    }, []);

    const peinture = peintures[props.id];

    if(peinture !== undefined){ 
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/peinture/layouts/layout_peinture_1.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Vendredi peinture</h1>
                <h2 class="scene1_h2">{peinture.titre}</h2>
                <img src={`/peinture/images/${peinture.url_photo}`} alt={peinture.titre} id="scene1_img" ></img>
                <div id="real">
                    <p>Peinte par <span>{peinture.peintre.nom}</span></p>
                    <img src={`/peinture/images/${peinture.peintre.url_photo}`} alt={peinture.peintre.nom} class="scene1_img" ></img>
                </div>
                <div id="desc">
                    <p>Peinture réalisée en <span>{peinture.date}</span></p>
                    <p>Elle est issue du mouvement du {peinture.mouvement_artistique}</p>
                </div>
                
            </div>
        );
    }else{
        return null;
    }
}
  
export default Peinture1;