import '../art-css/scene1.css';
import logo from '../logo_arteshow_blanc.png';
// import livres from '../art-json/livres.json';
import React, { useEffect, useState } from 'react';

const Livre1 = (props) => {
    const [livres, setLivres] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceIJuMPgqG?indent=2')
            .then((res) => res.json())
            .then((data) => setLivres(data));
    }, []);
    
    const livre = livres[props.id];

    if(livre !== undefined){    
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/litterature/layouts/layout_litterature_1.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Mercredi lecture</h1>
                <h2 id="titre" class="scene1_h2">{livre.titre}</h2>
                <img src={`/litterature/images/${livre.url_photo}`} alt={livre.titre} id="scene1_img" ></img>
                <div id="real">
                    <h3>Ecrit par <span>{livre.auteur.nom}</span></h3>
                    <img src={`/litterature/images/${livre.auteur.url_photo}`} alt={livre.auteur.nom} class="scene1_img" ></img>
                </div>
            <div id="desc">
                <p>Livre sorti en <span>{livre.date}</span></p>
                    <ul>
                        {livre.genres.length > 1 ? <p id="genre">Genres :</p> : <p id="genre">Genre :</p>}                        
                        {livre.genres.map( (genre) => (
                            <li key={genre}>
                                <p>{genre}</p>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }else{
        return null;
    }
}

export default Livre1;