import '../art-css/ecran_debut_fin.css';
import logo from '../logo_arteshow_blanc.png';

const EcranFin = () => {

    return(
        <div id="div_background">
            <span id="content">
                <h2 class="scenese_h2">ART'e-show, c'est fini pour aujourd'hui !</h2>
                <p class="end">Retrouvez le replay du live dès demain sur notre chaine Youtube :
                https://www.youtube.com/channel/UC5v_TX38H3N57QmBTAWFgGw</p>
                <img src={logo} alt="Logo" class="scenese_img"></img>
            </span>
        </div>
    );
}

export default EcranFin;