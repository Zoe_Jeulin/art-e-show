import '../art-css/scene2_sculpture.css';
import logo from '../logo_arteshow_blanc.png';
// import sculptures from '../art-json/sculptures.json';
import React, { useEffect, useState } from 'react';

const Sculpture2 = (props) => {
    const [sculptures, setSculptures] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceBIYtpmKW?indent=2')
            .then((res) => res.json())
            .then((data) => setSculptures(data));
    }, []);

    const sculpture = sculptures[props.id];
    
    if(sculpture !== undefined){
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/sculpture/layouts/layout_sculpture_2.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Dimanche sculpture</h1>
                <div class="div_contenu_map">
                    <div class="div_map">
                        <iframe className="map" src={sculpture.url_map} title="Carte"></iframe>
                    </div>
                    <div class="div_nom_lieu">
                        <p className="nom_lieu">Située à {sculpture.lieu}</p>
                    </div>
                </div> 
            </div>
        );
    }else{
        return null;
    }
}

export default Sculpture2;