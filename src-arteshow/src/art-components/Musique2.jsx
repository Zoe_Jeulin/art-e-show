import '../art-css/scene2_musique.css';
import logo from '../logo_arteshow_blanc.png';
// import musiques from '../art-json/musiques.json';
import React, { useEffect, useState } from 'react';

const Musique2 = (props) => {
    const [musiques, setMusiques] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/cjkBUReLfS?indent=2')
            .then((res) => res.json())
            .then((data) => setMusiques(data));
    }, []);

    const musique = musiques[props.id];
    
    if(musique !== undefined){
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/musique/layouts/layout_musique_2.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Jeudi musique</h1>
                <div id="div_clip_scene2">
                    <iframe src={musique.clip} alt={musique.titre} title="Clip"></iframe>
                </div>                
            </div>
        );
    }else{
        return null;
    }
}

export default Musique2;