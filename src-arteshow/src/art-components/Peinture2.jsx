import '../art-css/scene2_peinture.css';
import logo from '../logo_arteshow_blanc.png';
// import peintures from '../art-json/peintures.json';
import React, { useEffect, useState } from 'react';

const Peinture2 = (props) => {
    const [peintures, setPeintures] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceOGzVdNgy?indent=2')
            .then((res) => res.json())
            .then((data) => setPeintures(data));
    }, []);

    const peinture = peintures[props.id];

    if(peinture !== undefined){ 
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/peinture/layouts/layout_peinture_2.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Vendredi peinture</h1>
                <div class="div_contenu_map">
                    <div class="div_map">
                        <iframe className="map" src={peinture.url_map}  title="Carte"></iframe>
                    </div>
                    <div class="div_nom_lieu">
                        <p className="nom_lieu">Située au {peinture.lieu}</p>
                    </div>
                </div> 
            </div>
        );
    }else{
        return null;
    }
}
  
export default Peinture2;