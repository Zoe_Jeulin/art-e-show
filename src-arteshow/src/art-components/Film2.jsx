import '../art-css/scene2_film.css';
import logo from '../logo_arteshow_blanc.png';
// import films from '../art-json/films.json';
import React, { useEffect, useState } from 'react';

const Film2 = (props) => {
    const [films, setFilms] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceAcVUUGmq?indent=2')
            .then((res) => res.json())
            .then((data) => setFilms(data));
    }, []);

    const film = films[props.id];

    if(film !== undefined){
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/cinema/layouts/layout_cinema_2.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Mardi ciné</h1>
                <div id="ba">
                    <iframe src={film.trailer} alt={film.titre} title="Bande-annonce"></iframe>
                </div>
                
                <h3>Acteurs principaux :</h3>
                    <ul id="actors">
                        {film.acteurs.map( (acteur) => (
                            <li key={acteur.nom}>
                                <img class ="img-actor" src={`/cinema/images/acteurs/${acteur.url_photo}`} alt={acteur.nom}></img>
                                <p>{acteur.nom}</p>
                            </li>
                        ))}
                    </ul>
            </div>
        );
    }else{
        return null;
    } 
}
  
export default Film2;