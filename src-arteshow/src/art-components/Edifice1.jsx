import '../art-css/scene1.css';
import logo from '../logo_arteshow_blanc.png';
// import edifices from '../art-json/edifices.json';
import React, { useEffect, useState } from 'react';

const Edifice1 = (props) => {
    const [edifices, setEdifices] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceCfyHMAya?indent=2')
            .then((res) => res.json())
            .then((data) => setEdifices(data));
    }, []);
    
    const edifice = edifices[props.id];

    if(edifice !== undefined){
        return (
        <div id="div_background" style={{backgroundImage: "url(\"/architecture/layouts/layout_architecture_1.png\")"}}>
            <img src={logo} alt="Logo" class="logo"></img>
            <h1>Lundi archi'</h1>
            <h2 class="scene1_h2">{edifice.titre}</h2>
            <img src={`/architecture/images/${edifice.url_photo}`} alt={edifice.photo} id="scene1_img"></img>
            <div id="artistes">
                <ul>
                    {edifice.architectes.length > 1 ? <h4>Les architectes</h4> : <h4>L'architecte</h4>}
                    {edifice.architectes.map( (architecte) => architecte.url_photo!=null ?
                        (
                            <li key={architecte.nom}>
                                <p>{architecte.nom}</p>
                                <img src={`/architecture/images/${architecte.url_photo}`} alt={architecte.nom} class="scene1_img" />
                            </li>
                        ):(
                            <li key={architecte.nom}>
                                <p>{architecte.nom}</p>                    
                            </li>
                        )
                    )}
                    </ul>
            </div>
            <div id="desc">
                <p>Date de construction <span>{edifice.date}</span></p>
            </div>
        </div>
        );
    }else{
        return null;
    }
}

export default Edifice1;