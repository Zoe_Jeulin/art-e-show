import '../art-css/scene1.css';
import logo from '../logo_arteshow_blanc.png';
// import sculptures from '../art-json/sculptures.json';
import React, { useEffect, useState } from 'react';

const Sculpture1 = (props) => {
    const [sculptures, setSculptures] = useState([]);

    useEffect(() => {
        fetch('http://www.json-generator.com/api/json/get/ceBIYtpmKW?indent=2')
            .then((res) => res.json())
            .then((data) => setSculptures(data));
    }, []);

    const sculpture = sculptures[props.id];

    if(sculpture !== undefined){ 
        return (
            <div id="div_background" style={{backgroundImage: "url(\"/sculpture/layouts/layout_sculpture_1.png\")"}}>
                <img src={logo} alt="Logo" class="logo"></img>
                <h1>Dimanche sculpture</h1>
                <h2 id="titre" class="scene1_h2">{sculpture.titre}</h2> 
                <img src={`/sculpture/images/${sculpture.url_photo}`} alt={sculpture.titre} id="scene1_img" />
                <div id="real">
                    <p>Artiste : <span>{sculpture.sculpteur.nom}</span></p>
                    <img src={`/sculpture/images/${sculpture.sculpteur.url_photo}`} alt={sculpture.sculpteur.nom} class="scene1_img" />
                </div>                
                <div id="desc">
                    <p>Réalisé en <span>{sculpture.date}</span></p>
                </div>            
            </div>
        );
    }else{
        return null;
    }
}

export default Sculpture1;