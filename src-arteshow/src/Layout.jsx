import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import EcranDebut from './art-components/EcranDebut.jsx';
import EcranFin from './art-components/EcranFin.jsx';
import EcranSondage from './art-components/EcranSondage.jsx';
import Edifice1 from './art-components/Edifice1';
import Edifice2 from './art-components/Edifice2';
import Film1 from './art-components/Film1';
import Film2 from './art-components/Film2';
import Livre1 from './art-components/Livre1';
import Musique1 from './art-components/Musique1';
import Musique2 from './art-components/Musique2';
import Peinture1 from './art-components/Peinture1';
import Peinture2 from './art-components/Peinture2';
import Piece1 from './art-components/Piece1';
import Sculpture1 from './art-components/Sculpture1';
import Sculpture2 from './art-components/Sculpture2';
import './layout.css';

const Layout = (props) => (
    <div id="div_layout">
        <Router>
            <Switch>
                <Route path="/architecture/1">
                    <Edifice1 id={props.id} />
                </Route>
                <Route path="/architecture/2">
                    <Edifice2 id={props.id} />
                </Route>

                <Route path="/cinema/1">
                    <Film1 id={props.id} />
                </Route>
                <Route path="/cinema/2">
                    <Film2 id={props.id} />
                </Route>

                <Route path="/litterature/1">
                    <Livre1 id={props.id} />
                </Route>

                <Route path="/musique/1">
                    <Musique1 id={props.id} />
                </Route>
                <Route path="/musique/2">
                    <Musique2 id={props.id} />
                </Route>
                
                <Route path="/peinture/1">
                    <Peinture1 id={props.id} />
                </Route>
                <Route path="/peinture/2">
                    <Peinture2 id={props.id} />
                </Route>

                <Route path="/sculpture/1">
                    <Sculpture1 id={props.id} />
                </Route>
                <Route path="/sculpture/2">
                    <Sculpture2 id={props.id} />
                </Route>

                <Route path="/theatre/1">
                    <Piece1 id={props.id} />
                </Route>

                <Route path="/:art/start/:heures/:minutes">
                    <EcranDebut />
                </Route>
                <Route path="/end">
                    <EcranFin />
                </Route>
                <Route path="/:art/sondage/:choix1/:choix2">
                    <EcranSondage />
                </Route>
            </Switch>
        </Router>
    </div>
);

export default Layout;